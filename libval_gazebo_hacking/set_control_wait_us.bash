#!/bin/bash
ruby -e "print [$2].pack('d')" \
  | dd of=$1 seek=$((0x84f00)) bs=1 count=8 status=none
dd if=$1 skip=$((0x84f00)) bs=1 count=8 status=none \
  | ruby -e "puts STDIN.read.unpack('d')"
