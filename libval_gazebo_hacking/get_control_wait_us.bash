#!/bin/bash
dd if=$1 skip=$((0x84f00)) bs=1 count=8 status=none \
  | ruby -e "puts STDIN.read.unpack('d')"
