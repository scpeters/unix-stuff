# libval_gazebo_hacking

There are some scripts here for hacking the binary `libval_gazebo.so` in the
[val-gazebo deb](http://srcsim.gazebosim.org/src/pool/main/v/val-gazebo/val-gazebo_4.1.0.0-2016.12.07.23.54.28-14f009a4_amd64.deb)
hosted at [srcsim.gazebosim.org/src/](http://srcsim.gazebosim.org/src/).
This is needed since we don't currently have build scripts to make new debs.
There are two values for setting the delay in us for controller synchronization,
one waits for sensor data, and the other waits for a control command.
The default values are:

~~~
$ get_control_wait_us.bash /opt/nasa/indigo/lib/libval_gazebo.so
200000.0
$ get_sensor_wait_us.bash /opt/nasa/indigo/lib/libval_gazebo.so
1000
~~~

New values can be set with the `set_*` scripts:

~~~
$ sudo set_control_wait_us.bash /opt/nasa/indigo/lib/libval_gazebo.so 123
123.0
$ sudo set_sensor_wait_us.bash /opt/nasa/indigo/lib/libval_gazebo.so 800
800
~~~

## Data types

The controller wait time is stored as a double-precision floating point number,
while the sensor wait time is divided by 10 and then stored as an
unsigned 8-bit integer.
This means the sensor wait times can be in the range [0, 10, 20, ..., 2550].

The value will lose precision smaller than 10:
~~~
$ sudo set_sensor_wait_us.bash /opt/nasa/indigo/lib/libval_gazebo.so 123
120
~~~

You should be careful of overflow because the script is not:

~~~
$ sudo set_sensor_wait_us.bash /opt/nasa/indigo/lib/libval_gazebo.so 2560
0
~~~
