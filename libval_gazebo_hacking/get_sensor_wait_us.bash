#!/bin/bash
dd if=$1 skip=$((0x7346D)) bs=1 count=1 status=none \
  | ruby -e "puts STDIN.read.unpack('C')[0]*10"
