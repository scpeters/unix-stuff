#!/bin/bash
ruby -e "print [$2 / 10].pack('C')" \
  | dd of=$1 seek=$((0x7346D)) bs=1 count=1 status=none
dd if=$1 skip=$((0x7346D)) bs=1 count=1 status=none \
  | ruby -e "puts STDIN.read.unpack('C')[0]*10"
