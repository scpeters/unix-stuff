## gazebo8 Dockerfile

gazebo8 won't work on trusty, but we can use it from a docker container instead.
Use build.bash to build and `./run.bash bash` to run.
The Dockerfile clones gazebo and initializes a catkin workspace.
After running, `catkin build -i` should start the build.

### nvidia-docker

One of the pre-requisites for this Dockerfile is the
[nvidia-docker plugin](https://github.com/NVIDIA/nvidia-docker),
which must be installed on the host.
This plugin allows the host video drivers to be used in docker containers
without installing any drivers in the containers themselves.
Otherwise, matching drivers must be installed, which is a hassle.
The nvidia-docker [system requirements](https://github.com/NVIDIA/nvidia-docker/wiki/Installation)
include an nvidia driver >= 340.29 and the `nvidia-modprobe` package.
Once the system requirements are met
(and the system rebooted if new drivers were installed to be safe),
the plugin can be
[installed from a deb](https://github.com/NVIDIA/nvidia-docker/releases)
using `dpkg -i`.
The system should be restarted again after installing the plugin.

To verify that the plugin is running, check that it has started up correctly:

~~~
$ sudo tail /var/log/upstart/nvidia-docker.log
/usr/bin/nvidia-docker-plugin | 2016/07/01 11:52:29 Loading NVIDIA unified memory
/usr/bin/nvidia-docker-plugin | 2016/07/01 11:52:29 Loading NVIDIA management library
/usr/bin/nvidia-docker-plugin | 2016/07/01 11:52:29 Discovering GPU devices
/usr/bin/nvidia-docker-plugin | 2016/07/01 11:52:29 Provisioning volumes at /var/lib/nvidia-docker/volumes
/usr/bin/nvidia-docker-plugin | 2016/07/01 11:52:29 Serving plugin API at /var/lib/nvidia-docker
/usr/bin/nvidia-docker-plugin | 2016/07/01 11:52:29 Serving remote API at localhost:3476
~~~

To verify that the plugin is functioning, try querying for command-line arguments:

~~~
$ curl -s http://localhost:3476/docker/cli
--volume-driver=nvidia-docker --volume=nvidia_driver_352.63:/usr/local/nvidia:ro --device=/dev/nvidiactl --device=/dev/nvidia-uvm --device=/dev/nvidia0
~~~

If that returns values that look like docker command-line arguments,
then try running a minimal test with the `ubuntu:trusty` image.
The plugin should mount the nvidia drivers from the host at `/usr/local/nvidia`,
and you can run the `nvidia-smi` command to confirm.

~~~
$ docker run --rm -it \
    $(curl -s http://localhost:3476/docker/cli) \
    ubuntu:trusty \
    /bin/bash
# export LD_LIBRARY_PATH=/usr/local/nvidia/lib:/usr/local/nvidia/lib64
# /usr/local/nvidia/bin/nvidia-smi
Fri Jul  1 19:24:33 2016       
+------------------------------------------------------+                       
| NVIDIA-SMI 352.63     Driver Version: 352.63         |                       
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce GTX 650     Off  | 0000:01:00.0     N/A |                  N/A |
| 21%   31C    P8    N/A /  N/A |    118MiB /  1023MiB |     N/A      Default |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID  Type  Process name                               Usage      |
|=============================================================================|
|    0                  Not Supported                                         |
+-----------------------------------------------------------------------------+
~~~

Once the plugin has been used at least once, it should appear in docker info:

~~~
$ docker info
...
Plugins: 
 Volume: local nvidia-docker
 Network: host bridge null
...
~~~

It should also have created a docker volume after that first test:

~~~
$ docker volume ls
DRIVER              VOLUME NAME
nvidia-docker       nvidia_driver_352.63
~~~

