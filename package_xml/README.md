# Extra package.xml files for plain cmake packages

This folder contains several package.xml files that I use for
building the [Gazebo simulator](http://gazebosim.org)
and its dependencies.

## Reasons I like to use catkin

I have found [catkin workspaces](http://wiki.ros.org/catkin/workspaces)
to be a useful development tool for non-catkin CMake packages,
for the following reasons:

* It makes it easy to use software installed to a local folder by managing all
the environment variables in a setup.sh file.
No more setting `LD_LIBRARY_PATH` in my `.bashrc`!

* It is also easy to build groups of packages together.
I have some workspaces with gazebo built against the system-installed
versions of dependencies, and other workspaces with custom
versions of sdformat or physics engines.

* Of course you can also build ROS packages against
the custom version of Gazebo, since ROS uses catkin.

The following is a brief tutorial on building Gazebo
in a catkin workspace on a Unix-based operating system
(tested by the author on Ubuntu Linux and Mac OS X).

## Installing catkin-pkg

Using catkin requires the python `catkin-pkg` to be installed:

~~~
# Ubuntu
sudo apt-get install python-catkin-pkg
# Others
sudo pip install catkin-pkg
~~~

## Building Gazebo against system installs of dependencies

Make sure that you have installed the Gazebo dependencies
(see the [Install Gazebo from source tutorial](http://gazebosim.org/tutorials?tut=install_from_source&cat=install)).
Then create a catkin workspace `src` folder.
I personally use multiple catkin workspaces,
and I keep them in `~/ws`.
For this example, I would name the workspace `~/ws/gazebo`.

~~~
export WS=$HOME/ws/gazebo
mkdir -p ${WS}/src
~~~

Clone catkin and gazebo into the `src` folder:

~~~
cd ${WS}/src
git clone https://github.com/ros/catkin.git
hg clone https://bitbucket.org/osrf/gazebo
~~~

Choose your desired branch of gazebo (`hg up gazebo5` or whatever).
Download [package_gazebo.xml](https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_gazebo.xml)
and copy it into the cloned gazebo source folder as `package.xml`:

~~~
curl https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_gazebo.xml > ${WS}/src/gazebo/package.xml
~~~

From the workspace folder (`~/ws/gazebo` or `${WS}` in this example),
invoke `catkin_make_isolated`:

~~~
cd ${WS}
./src/catkin/bin/catkin_make_isolated
~~~

This will create a gazebo build folder in `${WS}/build_isolated/gazebo/devel`
and install gazebo to `${WS}/devel_isolated/gazebo`.
Importantly, it also creates numerous shell scripts that
set environment variables such as `PATH`, `LD_LIBRARY_PATH`,
and `PKG_CONFIG_PATH` so that the installed software can be
run and developed against:

The following sources a bash shell script for the workspace
that was just built.

~~~
$ . ${WS}/devel_isolated/setup.bash
$ which gazebo
~/ws/gazebo/devel_isolated/gazebo/bin/gazebo
$ pkg-config --cflags-only-I gazebo
-I/Users/scpeters/ws/gazebo/devel_isolated/gazebo/include/gazebo-4.1 ...
~~~

To run `make test` for gazebo after the local installation:

~~~
. ${WS}/devel_isolated/setup.bash
cd ${WS}/build_isolated/gazebo/devel
test/integration/INTEGRATION_physics
make test
~~~

## Building Gazebo against source builds of dependencies

Catkin makes it simple to build gazebo against source builds of dependencies.
For this example, I will show how to build gazebo against sdformat.
Create a new catkin workspace folder:

~~~
export WS=$HOME/ws/gazebo_sdformat
mkdir -p ${WS}/src
~~~

The next step is to clone the source code into the `src` folder.
If already have a clone of gazebo on your system, I recommend copying
rather than cloning to save time and bandwidth, since it is so large
(catkin and sdformat are much smaller).

~~~
cd ${WS}/src
git clone https://github.com/ros/catkin.git
# If you do not already have a clone of gazebo:
hg clone https://bitbucket.org/osrf/gazebo
# If you do (for example in ~/ws/gazebo/src/gazebo):
cp -R ~/ws/gazebo/src/gazebo ${WS}/src/gazebo
~~~

Choose the desired branch for sdformat and gazebo.
Then copy in the package.xml files:

~~~
curl https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_gazebo.xml > ${WS}/src/gazebo/package.xml
curl https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_sdformat.xml > ${WS}/src/sdformat/package.xml
~~~

The package.xml files allow catkin to recognize each folder as a catkin package,
but also to learn about dependencies between packages so they can be built
in the correct order.
Note that the [package_gazebo.xml](https://bitbucket.org/scpeters/unix-stuff/src/master/package_xml/package_gazebo.xml#cl-53)
file `depends_on` sdformat.

The remaining steps are the same: invoke `catkin_make_isolated`
and source the setup script.

~~~
$ cd ${WS}
$ ./src/catkin/bin/catkin_make_isolated
...
$ . ${WS}/devel_isolated/setup.bash
$ pkg-config --cflags-only-I gazebo
-I/Users/scpeters/ws/gazebo_sdformat/devel_isolated/gazebo/include/gazebo-5.0
-I/usr/local/include
-I/Users/scpeters/ws/gazebo_sdformat/devel_isolated/sdformat/include/sdformat-2.2
...
~~~

## Building sdformat against source builds of urdfdom

Building sdformat against urdfdom is a special case,
since package.xml files for urdfdom are stored in the release
repositories, so there is no copy here.
Instead, I have a script called
[package_ros.xml.sh](https://bitbucket.org/scpeters/unix-stuff/src/master/package_xml/package_ros.xml.sh).

After cloning the repositories and getting the sdformat package.xml:

~~~
export WS=$HOME/ws/sdformat_urdfdom
mkdir -p ${WS}/src
cd ${WS}/src
git clone https://github.com/ros/catkin.git
hg clone https://bitbucket.org/osrf/sdformat
git clone https://github.com/ros/urdfdom_headers.git
git clone https://github.com/ros/urdfdom.git
git clone https://github.com/ros/console_bridge.git
curl https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_sdformat.xml > ${WS}/src/sdformat/package.xml
~~~

run the script to get the package.xml file for
urdfdom, urdfdom_headers, and console_bridge
with specified version numbers:

~~~
cd ${WS}
wget https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_ros.xml.sh
. package_ros.xml.sh urdfdom 0.2.3 > ${WS}/src/urdfdom
. package_ros.xml.sh urdfdom_headers 0.2.3 > ${WS}/src/urdfdom_headers
. package_ros.xml.sh console_bridge 0.1.0 > ${WS}/src/console_bridge
~~~

## Building drcsim against a source build of gazebo

The default behavior of `catkin_make_isolated` is to install each package
to separate folders.
Some packages, such as `drcsim`, require installation to a common prefix.
This can be done with `catkin_make_isolated --install` and will
be illustrated with by instructions for building `drcsim`
against source builds of `gazebo` and the Dynamics, Animation, and
Robotics Toolkit ([DART](http://dartsim.github.io)).

Make a workspace, clone the source code, and insert the package.xml files.
Note that catkin will not be cloned, since this build will include
ROS packages and the ROS version of catkin will be used.

~~~
export WS=$HOME/ws/drcsim_gazebo_dart
export ROS_DISTRO=indigo
cd ${WS}
mkdir -p ${WS}/src
cd ${WS}/src
git clone https://github.com/dartsim/dart.git
hg clone https://bitbucket.org/osrf/gazebo
git clone https://github.com/ros-simulation/gazebo_ros_pkgs.git -b ${ROS_DISTRO}-devel
hg clone https://bitbucket.org/osrf/drcsim
curl https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_dart-core.xml > ${WS}/src/dart/package.xml
curl https://bitbucket.org/scpeters/unix-stuff/raw/master/package_xml/package_gazebo.xml > ${WS}/src/gazebo/package.xml
~~~

Then source the ROS setup.sh file and build. Note that DART can be built
with the optional cmake parameter `BUILD_CORE_ONLY`, which reduces
the number of installed dependencices.
This can be specified for catkin at the command-line.

~~~
. /opt/ros/${ROS_DISTRO}/setup.bash
cd ${WS}
catkin_make_isolated --install --cmake-args -DBUILD_CORE_ONLY=ON
~~~

~~~
. ${WS}/install_isolated/share/drcsim/setup.sh
roslaunch drcsim_gazebo atlas.launch extra_gazebo_args:="--verbose -e dart"
~~~
