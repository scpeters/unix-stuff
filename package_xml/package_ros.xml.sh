# Get version number from git repo
# Version sort string from http://stackoverflow.com/a/4495368
VERSION=`
git ls-remote --tags \
  https://github.com/ros/$1.git \
  | sed -e 's@.*refs/tags/@@' \
  | grep -v '[a-zA-Z_]' \
  | sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n \
  | tail -1
`
echo Generating package.xml for ros/$1 $VERSION >&2
curl -s https://raw.githubusercontent.com/ros-gbp/$1-release/master/hydro/package.xml \
  | sed -e "s@:{version}@$VERSION@"
